#!/usr/bin/env python
from flask_script import Manager
from flask_script.commands import Server, Shell, ShowUrls, Clean
from flask_migrate import MigrateCommand
from flask_security.cli import users_deactivate, users_activate, users_create, roles_add, roles_create, roles_remove

from app import create_app, db
from app.commands import InitDB
from config import Config

config = Config()
app = create_app(config)

manager = Manager(app)
manager.add_command("shell", Shell(use_ipython=True))
manager.add_command("runserver", Server(use_reloader=True))
manager.add_command("show_urls", ShowUrls())
manager.add_command("clean", Clean())

manager.add_command('init_db', InitDB())
manager.add_command('db', MigrateCommand)

#manager.add_command('create_user', users_create)
#manager.add_command('deactivate_user', users_deactivate)
#manager.add_command('activate_user', users_activate)

#manager.add_command('create_role', roles_create)
#manager.add_command('add_role', roles_add)
#manager.add_command('remove_role', roles_remove)


with app.app_context():
    db.metadata.create_all(bind=db.engine)


if __name__ == "__main__":
    manager.run()