import os
import logging

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'MYSECRET'
    JWT_SECRET_KEY = SECRET_KEY
    SITE_NAME = 'AURORA'
    SITE_ROOT_URL = 'http://127.0.0.1:5000'
    LOG_LEVEL = logging.DEBUG
    DEBUG=True

    LANGUAGES = ['en', 'de']

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED = True
    CSRF_SESSION_KEY = "secret"

    # Configured for Gmail
    #DEFAULT_MAIL_SENDER = 'Admin < username@example.com >'
    #MAIL_SERVER = 'smtp.gmail.com'
    #MAIL_PORT = 465
    #MAIL_USE_SSL = True
    #MAIL_USERNAME = 'username@gmail.com'
    #MAIL_PASSWORD = '*********'

    # Flask-Security setup
    SECURITY_USER_IDENTITY_ATTRIBUTES = ('username','email')
    #SECURITY_EMAIL_SENDER = 'Security < security@example.com >'
    SECURITY_LOGIN_WITHOUT_CONFIRMATION = True
    SECURITY_REGISTERABLE = False
    SECURITY_RECOVERABLE = False
    SECURITY_URL_PREFIX = '/auth'
    SECUIRTY_POST_LOGIN = '/'
    SECURITY_TRACKABLE = True
    SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
    # import uuid; salt = uuid.uuid4().hex
    SECURITY_PASSWORD_SALT = '05cb26f409ca4de9b9ed8cef5a631b7a'

    SECURITY_MSG_USER_DOES_NOT_EXIST = ('Your username and password do not match our records', 'error')
    SECURITY_MSG_INVALID_EMAIL_ADDRESS = ('Your username and password do not match our records', 'error')
    SECURITY_MSG_INVALID_PASSWORD = ('Your username and password do not match our records', 'error')

    CKEDITOR_SERVE_LOCAL = True

    RESTPLUS_MASK_SWAGGER = False

    #DEBUG_TB_PANELS = 'flask_debugtoolbar_sqlalchemy.SQLAlchemyPanel'
    # CACHE
    #CACHE_TYPE = 'simple'