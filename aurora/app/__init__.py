from flask import Flask, render_template, url_for
from flask_bootstrap import Bootstrap
from flask_ckeditor import CKEditor
from flask_migrate import Migrate
from flask_security import Security, SQLAlchemyUserDatastore
from flask_babel import Babel
#from flask_debugtoolbar import DebugToolbarExtension
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager


#from ..config import Config
from .auth.forms import ExtendedLoginForm
from .models.accounts import  Role, User
from .admin import configure_admin
from .db import db

ckeditor = CKEditor()
bootstrap = Bootstrap()
migrate = Migrate()
#toolbar = DebugToolbarExtension()
babel = Babel()
ma = Marshmallow()
jwt_manager = JWTManager()


def create_app(config_class):
    app = Flask(__name__)
    app.config.from_object(config_class)

    register_errorhandlers(app)
    register_template_filter(app)

    # initialize extensions
    bootstrap.init_app(app)
    ckeditor.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    ma.init_app(app)
    babel.init_app(app)
    #toolbar.init_app(app)
    jwt_manager.init_app(app)

    # security
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    app.security = Security(app, user_datastore)

    # import blueprints
    register_blueprints(app)
    configure_admin(app, db)

    return app


def register_blueprints(app):
    from .main import main as main_bp
    app.register_blueprint(main_bp)

    from .projects import project_bp
    app.register_blueprint(project_bp)

    from .tags import tag_bp
    app.register_blueprint(tag_bp)

    from .references import references_bp
    app.register_blueprint(references_bp)

    from .vulns import vuln_bp
    app.register_blueprint(vuln_bp)

    from .contacts import contact_bp
    app.register_blueprint(contact_bp)

    from .api import api_bp
    app.register_blueprint(api_bp)


def register_errorhandlers(app):
    """Register error handlers with the Flask application."""
    @app.errorhandler(403)
    def forbidden(error):
        return render_template('errors/403.html', title='Forbidden'), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('errors/404.html', title='Page Not Found'), 404

    @app.errorhandler(500)
    def internal_server_error(error):
        return render_template('errors/500.html', title='Server Error'), 500


def register_template_filter(app):
    @app.template_filter()
    def format_date_for_table(value, format="%d.%m.%Y"):
        if value is None:
            return ""
        return value.strftime(format)



