from flask import render_template, flash, redirect, url_for, request
from flask_security import current_user, login_required, roles_accepted

from ..db import db
from ..vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, VULN_ADMIN_ROLE_NAME
from ..models.defaultvulns import DefaultVuln

from . import vuln_bp
from .forms import DefaultVulnForm


@vuln_bp.route('/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, VULN_ADMIN_ROLE_NAME)
def list():
    vulns = DefaultVuln.query.all()
    return render_template('vuln_list.html', vulns=vulns, title="Vulnerability templates")


@vuln_bp.route('/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, VULN_ADMIN_ROLE_NAME)
def create():
    form = DefaultVulnForm()
    if request.method == 'POST' and form.validate_on_submit():
        vuln = DefaultVuln(
            name=form.name.data,
            description=form.description.data,
            solution = form.solution.data,
            impact = form.impact.data,
            likelihood = form.likelihood.data)
        try:
            db.session.add(vuln)
            db.session.commit()
            flash('You have successfully added a new vulnerability template.', 'success')
        except:
            flash('Error: vulnerability template already exists.', 'error')

        return redirect(url_for('defaultvulns.list'))

    return render_template('vuln.html', form=form, title="New Vulnereability template")


@vuln_bp.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, VULN_ADMIN_ROLE_NAME)
def edit(id):
    vuln = DefaultVuln.query.get_or_404(id)
    form = DefaultVulnForm(obj=vuln)
    if form.validate_on_submit():
        form.populate_obj(vuln)
        try:
            db.session.add(vuln)
            db.session.commit()
            flash('You have successfully added a new vulnerability template.', 'success')
        except:
            flash('Error: vulnerability template already exists.', 'error')

        return redirect(url_for('vulns.list'))

    form.name.data = vuln.name
    form.description.data = vuln.description
    form.solution.data = vuln.solution
    form.impact.data = vuln.impact
    form.likelihood.data = vuln.likelihood
    return render_template('vuln.html', form=form, title="Edit vulnerability template")


@vuln_bp.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, VULN_ADMIN_ROLE_NAME)
def delete(id):
    vuln = DefaultVuln.query.get_or_404(id)

    db.session.delete(vuln)
    db.session.commit()
    flash('You have successfully deleted the vulnerability template.', 'success')

    return redirect(url_for('vulns.list'))
