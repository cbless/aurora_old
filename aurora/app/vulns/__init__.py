from flask import Blueprint

vuln_bp = Blueprint('vulns', __name__, url_prefix='/vulns', template_folder='templates')

from .views import *

