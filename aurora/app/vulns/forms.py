from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, BooleanField
from wtforms_alchemy import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import DataRequired

from wtforms_alchemy import QuerySelectField

from ..models.core import ImpactLevel, LikelihoodLevel, Tag, Reference
from flask_ckeditor import CKEditorField
from flask_admin.form.widgets import Select2Widget

def impact_choices():
    return ImpactLevel.query.all()


def likelihood_choices():
    return LikelihoodLevel.query.all()


class DefaultVulnForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    impact = QuerySelectField('Impact',
                              query_factory=impact_choices,
                              render_kw = {'data-role': 'select2'})
    likelihood = QuerySelectField('Likelihood',
                                  query_factory=likelihood_choices,
                                  render_kw = {'data-role': 'select2'})
    exploit_available = BooleanField('Exploit available')
    description = CKEditorField('Description')
    solution = CKEditorField('Solution')

    tags = QuerySelectMultipleField(query_factory=lambda: Tag.query.all())
    references = QuerySelectMultipleField(query_factory=lambda: Reference.query.all())

    submit = SubmitField('Submit')

