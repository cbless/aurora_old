from ..db import db


project_assessors_association = db.Table('projects_assessors',
                             db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
                             db.Column('project_id', db.Integer, db.ForeignKey('projects.id')))


project_customer_contacts_association = db.Table('project_customer_contacts',
                             db.Column('contact_id', db.Integer, db.ForeignKey('contacts.id')),
                             db.Column('project_id', db.Integer, db.ForeignKey('projects.id')))


project_methodologies_association = db.Table('project_methodologies',
                           db.Column('methodology_id', db.Integer, db.ForeignKey('methodologies.id')),
                           db.Column('project_id', db.Integer, db.ForeignKey('projects.id')))


class Module(db.Model):
    __tablename__ = "modules"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text(), nullable=True)
    notes = db.Column(db.Text(), nullable=True)

    status_id = db.Column(db.Integer, db.ForeignKey('statuses.id'))
    status = db.relationship('Status')

    assigned_to_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    assigned_to = db.relationship('User')

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), nullable=False)
    project = db.relationship('Project', backref=db.backref("project module"))

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class Todo(db.Model):
    __tablename__ = "todos"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text(), nullable=True)
    notes = db.Column(db.Text(), nullable=True)

    status_id = db.Column(db.Integer, db.ForeignKey('statuses.id'))
    status = db.relationship('Status')

    assigned_to_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    assigned_to = db.relationship("User")

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), nullable=False)
    project = db.relationship('Project', backref=db.backref("project todos"))

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class Project(db.Model):
    __tablename__ = "projects"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    start_date = db.Column(db.DateTime())
    end_date = db.Column(db.DateTime())
    status_id = db.Column(db.Integer, db.ForeignKey('statuses.id'))
    status = db.relationship("Status")
    description = db.Column(db.Text())
    summary = db.Column(db.Text())
    note = db.Column(db.Text())

    project_manager_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    project_manager = db.relationship("User")

    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'))
    company = db.relationship("Company", foreign_keys=[company_id])

    customer_id = db.Column(db.Integer, db.ForeignKey('companies.id'))
    customer = db.relationship("Company", foreign_keys=[customer_id])

    customer_pm_id = db.Column(db.Integer, db.ForeignKey('contacts.id'))
    customer_pm = db.relationship("Contact")

    customer_contacts = db.relationship(
        'Contact',
        secondary=project_customer_contacts_association
    )

    assessors = db.relationship(
        'User',
        secondary=project_assessors_association
    )

    methodologies = db.relationship(
        'Methodology',
        secondary=project_methodologies_association
    )

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)