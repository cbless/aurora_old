
from ..db import db

methodologies_items_association = db.Table('methodologies_items',
                               db.Column('methodology_id', db.Integer, db.ForeignKey('methodologies.id')),
                               db.Column('methodologyitem_id', db.Integer, db.ForeignKey('methodologyitems.id')))

class MethodologyItem(db.Model):
    __tablename__ = "methodologyitems"
    id = db.Column(db.Integer, primary_key=True)
    identifier = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text())
    link = db.Column(db.String(2048), nullable=False)

    def __str__(self):
        return self.identifier

    def __unicode__(self):
        return self.identifier

    def __repr__(self):
        return self.identifier

    def __hash__(self):
        return hash(self.identifier)


class Methodology(db.Model):
    __tablename__ = "methodologies"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    description = db.Column(db.Text())
    link = db.Column(db.String(2048), nullable=False)
    items = db.relationship(
        'MethodologyItem',
        secondary=methodologies_items_association,
        backref=db.backref('methodologyitems', lazy='dynamic')
    )

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)