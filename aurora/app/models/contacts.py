from ..db import db


class Company(db.Model):
    __tablename__ = "companies"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(400), nullable=False)

    street = db.Column(db.String(400), nullable=True)
    zipcode = db.Column(db.String(10), nullable=True)
    city = db.Column(db.String(255), nullable=True)

    notes = db.Column(db.Text())

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class Contact(db.Model):
    __tablename__ = "contacts"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(10), nullable=True)
    firstname = db.Column(db.String(255), nullable=True)
    lastname = db.Column(db.String(255), nullable=False)

    # maybe i will redesign the following columns as reference table
    phone = db.Column(db.String(255), nullable=True)
    mobile = db.Column(db.String(255), nullable=True)
    email = db.Column(db.String(2048), nullable=True)

    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'))
    company = db.relationship("Company", backref="employee")

    street = db.Column(db.String(400), nullable=True)
    zipcode = db.Column(db.String(10), nullable=True)
    city = db.Column(db.String(255), nullable=True)

    notes = db.Column(db.Text())

    def __str__(self):
        return "{0} {1}".format(self.firstname, self.lastname)

    def __unicode__(self):
        return "{0} {1}".format(self.firstname, self.lastname)

    def __repr__(self):
        return "{0} {1}".format(self.firstname, self.lastname)

    def __hash__(self):
        return hash("{0} {1}".format(self.firstname, self.lastname))