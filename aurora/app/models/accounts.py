from flask_security import UserMixin, RoleMixin

from ..db import db

roles_users_association = db.Table('roles_users',
                       db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
                       db.Column('role_id', db.Integer, db.ForeignKey('roles.id')))


class User(db.Model, UserMixin):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    username = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255), nullable=False)
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.String(100))
    current_login_ip = db.Column(db.String(100))
    login_count = db.Column(db.Integer)

    contact_id = db.Column(db.Integer, db.ForeignKey('contacts.id'), nullable=True)
    contact = db.relationship("Contact")

    roles = db.relationship(
        'Role',
        secondary=roles_users_association,
        backref=db.backref('users', lazy='dynamic')
    )

    def __str__(self):
        return self.username

    def __repr__(self):
        return self.username

    def __unicode__(self):
        return self.username

    def __hash__(self):
        return hash(self.username)



class Role(db.Model, RoleMixin):
    __tablename__ = "roles"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.Text())

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)
