from ..db import db

vulnerability_references = db.Table('vulnerability_references',
                                   db.Column('ref_id', db.Integer, db.ForeignKey('references.id')),
                                   db.Column('vulnerability_id', db.Integer, db.ForeignKey('vulnerabilities.id')))


vulnerability_tags = db.Table('vulnerability_tags',
                             db.Column('tag_id', db.Integer, db.ForeignKey('tags.id')),
                             db.Column('vulnerability_id', db.Integer, db.ForeignKey('vulnerabilities.id')))


class Vulnerability(db.Model):
    __tablename__ = "vulnerabilities"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text())
    solution = db.Column(db.Text())

    impact_id = db.Column(db.Integer, db.ForeignKey('impactlevel.id'), nullable=False)
    impact = db.relationship('ImpactLevel', backref="Vulnerability impact")

    likelihood_id = db.Column(db.Integer, db.ForeignKey('likelihoodlevel.id'), nullable=False)
    likelihood = db.relationship('LikelihoodLevel', backref="Vulnerability likelihood")

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), nullable=False)
    project = db.relationship('Project', backref=db.backref("vuln_by_project"))

    tags = db.relationship(
        'Tag',
        secondary=vulnerability_tags,
        backref=db.backref('vulnTags', lazy='dynamic')
    )
    references = db.relationship(
        'Reference',
        secondary=vulnerability_references,
        backref=db.backref('vulnReferences', lazy='dynamic')
    )

    exploit_available = db.Column(db.Boolean)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

