from ..db import db


class ImpactLevel(db.Model):
    __tablename__ = "impactlevel"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    description = db.Column(db.Text())

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'RiskLevel' when saving other objects.
    def __hash__(self):
        return hash(self.name)


class LikelihoodLevel(db.Model):
    __tablename__ = "likelihoodlevel"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    description = db.Column(db.Text())

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'RiskLevel' when saving other objects.
    def __hash__(self):
        return hash(self.name)


class Status(db.Model):
    __tablename__ = "statuses"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    description = db.Column(db.Text())

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'RiskLevel' when saving other objects.
    def __hash__(self):
        return hash(self.name)


class Tag(db.Model):
    __tablename__ = "tags"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'Tag' when saving other objects
    def __hash__(self):
        return hash(self.name)


class Reference(db.Model):
    __tablename__ = "references"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    link = db.Column(db.String(2048), nullable=False)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'Reference' when saving other objects
    def __hash__(self):
        return hash(self.name)