from flask import abort, redirect, url_for, request
from flask_admin import helpers as admin_helpers
from flask_admin import Admin
from flask_admin.base import MenuLink

from ..models.accounts import Role, User
from ..models.core import ImpactLevel, LikelihoodLevel, Tag, Reference, Status
from ..models.projects import Project
from ..models.defaultvulns import DefaultVuln
from ..models.vulnerabilities import Vulnerability
from ..models.systems import System, Host, SystemType


from .views import *

# admin = Admin(name="Admin", base_template='base_layout_admin.html', template_mode='bootstrap3',)
admin = Admin(name="Admin", template_mode='bootstrap3',)


def configure_admin(app, db):
    admin.init_app(app)
    # Add model views
    admin.add_link(MenuLink(name='Back Home', url='/'))
    admin.add_view(AdminView(Role, db.session, category='Accounts'))
    admin.add_view(UserAdminView(User, db.session, category='Accounts'))
    admin.add_view(DefaultVulnAdminView(DefaultVuln, db.session, category='Templates'))
    admin.add_view(AdminView(Tag, db.session, category='Templates'))
    admin.add_view(AdminView(Reference, db.session, category='Templates'))
    admin.add_view(AdminView(ImpactLevel, db.session, category='Templates'))
    admin.add_view(AdminView(LikelihoodLevel, db.session, category='Templates'))
    admin.add_view(AdminView(Status, db.session, category='Templates'))
    admin.add_view(AdminView(SystemType, db.session, category='Templates'))
    admin.add_view(MethodologyAdminView (Methodology, db.session, category='Templates'))
    admin.add_view(MethodologyItemAdminView (MethodologyItem, db.session, category='Templates'))
    admin.add_view(ProjectAdminView(Project, db.session, category='Project'))
    admin.add_view(VulnerabilityAdminView (Vulnerability, db.session, category='Project'))
    admin.add_view(AdminView(System, db.session, category='Project'))
    admin.add_view(AdminView(Host, db.session, category='Project'))
    admin.add_view(CompanyAdminView(Company, db.session, category='CRM'))
    admin.add_view(ContactAdminView(Contact, db.session, category='CRM'))

    @app.security.context_processor
    def security_context_processor():
        return dict(
            admin_base_template=admin.base_template,
            admin_view=admin.index_view,
            h=admin_helpers,
            get_url=url_for
        )

