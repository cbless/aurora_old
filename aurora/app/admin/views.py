from flask_admin.form import SecureForm
from flask_admin.contrib.sqla import ModelView
from flask_security import current_user, utils
from wtforms.fields import PasswordField
from flask import abort, redirect, request, url_for
from flask_ckeditor import CKEditorField

from ..models.methodologies import Methodology, MethodologyItem
from ..models.projects import Module, Todo
from ..models.contacts import Company, Contact

# Customized admin interface
class CustomView(ModelView):
    form_base_class = SecureForm
    #list_template = 'list.html'
    create_template = 'admin/create.html'
    edit_template = 'admin/edit.html'

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        return True

    def inaccessible_callback(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))

    can_edit = True
    create_modal = False
    can_view_details = True
    view_details_modal = True
    can_export = False


# Create customized model view class
class AdminView(CustomView):
    can_export = True

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('Admin'):
            return True

        return False


class UserAdminView(AdminView):
    column_list = ['id', 'username', 'email', 'active']
    column_searchable_list = ['username', 'email']
    column_exclude_list = ['password']
    column_filters = column_list
    column_exclude_list = ['assigned user for todo', 'project manager', 'users']
    column_export_list = ['id', 'username', 'email', 'active', 'confirmed_at']
    # Don't include the standard password field when creating or editing a User (but see below)
    form_excluded_columns = ('password',)

    # On the form for creating or editing a User, don't display a field corresponding to the model's password field.
    # There are two reasons for this. First, we want to encrypt the password before storing in the database. Second,
    # we want to use a password field (with the input masked) rather than a regular text field.
    def scaffold_form(self):
        # Start with the standard form as provided by Flask-Admin. We've already told Flask-Admin to exclude the
        # password field from this form.
        form_class = super(UserAdminView, self).scaffold_form()

        # Add a password field, naming it "password2" and labeling it "New Password".
        form_class.password2 = PasswordField('New Password')

        return form_class

    # This callback executes when the user saves changes to a newly-created or edited User -- before the changes are
    # committed to the database.
    def on_model_change(self, form, model, is_created):
        # If the password field isn't blank...
        if len(model.password2):
            # ... then encrypt the new password prior to storing it in the database. If the password field is blank,
            # the existing password in the database will be retained.

            model.password = utils.encrypt_password(model.password2)


class DefaultVulnAdminView(AdminView):
    column_hide_backrefs = False
    column_list = ['id', 'name', 'impact', 'likelihood', 'tags', 'exploit_available']
    #column_editable_list = ['name', 'impact', 'likelihood']
    column_filters = ['name', 'impact', 'likelihood', 'tags', 'references', 'exploit_available']
    form_overrides = dict(description=CKEditorField, solution=CKEditorField)


class MethodologyAdminView(AdminView):
    inline_models = [MethodologyItem,]


class MethodologyItemAdminView(AdminView):
    can_create = False
    can_edit = False
    can_delete = False
    column_list = ['id', 'name', 'methodology']


class ProjectAdminView(AdminView):
    form_overrides = dict(description=CKEditorField, note=CKEditorField, summary=CKEditorField)
    inline_models = [Module,Todo, ]
    column_list = ['id', 'name', 'project_manager','start_date', 'end_date', 'status']


class VulnerabilityAdminView(AdminView):
    column_hide_backrefs = False
    column_list = ['id', 'name', 'impact', 'likelihood', 'tags', 'exploit_available']
    column_editable_list = ['name', 'impact', 'likelihood']
    column_filters = ['name', 'impact', 'likelihood', 'tags', 'exploit_available']
    form_overrides = dict(description=CKEditorField, solution=CKEditorField)




class CompanyAdminView(AdminView):
    form_overrides = dict(notes=CKEditorField)
    #inline_models = [Address, ]
    column_list = ['id', 'name']


class ContactAdminView(AdminView):
    form_overrides = dict(notes=CKEditorField)
    #inline_models = [Address, ]
    column_list = ['id', 'firstname', 'lastname', 'company']
