from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class TagForm(FlaskForm):
    """
    Form to add or edit a tag
    """
    name = StringField('Name', validators=[DataRequired()])
    submit = SubmitField('Save')