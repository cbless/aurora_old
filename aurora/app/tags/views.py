from flask import render_template, flash, redirect, url_for, request
from flask_security import current_user, login_required, roles_accepted

from ..db import db
from ..vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, TAG_ADMIN_ROLE_NAME
from ..models.core import Tag

from . import tag_bp
from .forms import TagForm


@tag_bp.route('/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, TAG_ADMIN_ROLE_NAME)
def list():
    tags = Tag.query.all()

    return render_template('tag_list.html', tags=tags, title="Tags")


@tag_bp.route('/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, TAG_ADMIN_ROLE_NAME)
def create():
    form = TagForm()
    if request.method == 'POST' and form.validate_on_submit():
        tag = Tag(name=form.name.data)
        try:
            db.session.add(tag)
            db.session.commit()
            flash('You have successfully added a new tag.', 'success')
        except:
            flash('Error: tag name already exists.', 'error')

        return redirect(url_for('tags.list'))

    return render_template('tag.html', action="Add", add_tag=True, form=form, title="Add Tag")


@tag_bp.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, TAG_ADMIN_ROLE_NAME)
def edit(id):
    tag = Tag.query.get_or_404(id)
    form = TagForm(obj=tag)
    if form.validate_on_submit():
        tag.name = form.name.data
        try:
            db.session.add(tag)
            db.session.commit()
            flash('You have successfully modified a tag.', 'success')
        except:
            flash('Error: tag name already exists.', 'error')

        return redirect(url_for('tags.list'))

    form.name.data = tag.name
    return render_template('tag.html', action="Edit", add_tag=False, form=form, title="Edit Tag")


@tag_bp.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, TAG_ADMIN_ROLE_NAME)
def delete(id):
    tag = Tag.query.get_or_404(id)

    db.session.delete(tag)
    db.session.commit()
    flash('You have successfully deleted the tag.', 'success')

    # redirect to the tag page
    return redirect(url_for('tags.list'))
