from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.projects import Project
from ...helper import find_projects_for_user, has_access
from ...db import db

from .. import project_bp
from ..forms import ProjectForm

@project_bp.route('/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def list():
    """
    List all projects the user has access to
    """
    user = current_user
    projects = find_projects_for_user(user)

    return render_template('project_list.html', projects=projects, title="Projects")


@project_bp.route('/create', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def create():
    form = ProjectForm()
    if request.method == 'POST' and form.validate_on_submit():
        project = Project(name=form.name.data,
                          description=form.description.data,
                          note=form.note.data,
                          summary=form.summary.data,
                          start_date=form.start_date.data,
                          customer=form.customer.data,
                          customer_pm=form.customer_pm.data,
                          customer_contacts=form.customer_contacts.data,
                          end_date=form.end_date.data,
                          status=form.status.data,
                          project_manager=form.project_manager.data,
                          assessors=form.assessors.data
                          )
        try:
            db.session.add(project)
            db.session.commit()
            flash('You have successfully added a new project.', 'success')
        except:
            flash('Error: project name already exists.', 'error')

        return redirect(url_for('projects.list'))

    return render_template('project.html', form=form, title="Add Project")


@project_bp.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def edit(id):
    if not has_access(id, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(id)
    form = ProjectForm(obj=project)
    if form.validate_on_submit():
        form.populate_obj(obj=project)
        try:
            db.session.add(project)
            db.session.commit()
            flash('You have successfully modified a project.', 'success')
        except:
            flash('Error: project name already exists.', 'error')

        return redirect(url_for('projects.list'))

    form.name.data = project.name
    form.description.data = project.description
    form.note.data = project.note
    form.summary.data = project.summary
    form.start_date.data = project.start_date
    form.end_date.data = project.end_date
    form.status.data = project.status
    form.project_manager.data = project.project_manager
    form.assessors.data = project.assessors
    form.customer.data = project.customer
    form.customer_pm.data = project.customer_pm
    form.customer_contacts.data = project.customer_contacts
    return render_template('project.html', form=form, title="Edit Project")


@project_bp.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def delete(id):
    project = Project.query.get_or_404(id)

    db.session.delete(project)
    db.session.commit()
    flash('You have successfully deleted the project.', 'success')

    return redirect(url_for('projects.list'))

