from flask import render_template, request, flash, redirect, url_for, abort, jsonify
from flask_security import current_user, login_required, roles_accepted

from ...db import db
from ...helper import has_access, find_users_by_project
from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.projects import Project, Todo

from .. import project_bp
from ..forms import TodoForm, TodoEditForm


@project_bp.route('/<int:pid>/todos/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def todos(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    todos = Todo.query.filter(Todo.project_id == pid).all()

    return render_template('todos/todolist.html', project=project, todos=todos, title="Project Todos")


@project_bp.route('/<int:pid>/todos/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def add_todo(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)

    form = TodoForm()

    if request.method == 'POST' and form.validate_on_submit():
        todo = Todo(name=form.name.data,
                    description=form.description.data,
                    notes=form.notes.data,
                    status=form.status.data,
                    assigned_to=form.assigned_to.data,
                    project=project
                    )
        try:
            db.session.add(todo)
            db.session.commit()
            flash('You have successfully added a new todo.', 'success')
        except:
            flash('Error: todo name already exists.', 'error')

        return redirect(url_for('projects.todos', pid=project.id))

    return render_template('todos/todo.html', form=form, project=project, title="Add Todo")


@project_bp.route('/<int:pid>/todos/edit/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def edit_todo(pid, id):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    todo = Todo.query.get_or_404(id)
    form = TodoEditForm()

    if form.validate_on_submit():
        form.populate_obj(todo)
        try:
            db.session.add(todo)
            db.session.commit()
            flash('You have successfully added a new todo item or task to project.', 'success')
        except:
            flash('Error: todo item / task already exists.', 'error')

        return redirect(url_for('projects.todos', pid=project.id))

    form.name.data = todo.name
    form.description.data = todo.description
    form.notes.data = todo.notes
    form.status.data = todo.status
    form.assigned_to.data = todo.assigned_to
    return render_template('todos/todo.html', form=form, project=project, title="Edit Todo")
