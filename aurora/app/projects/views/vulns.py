from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.projects import Project
from ...models.vulnerabilities import Vulnerability
from ...models.defaultvulns import DefaultVuln

from ...helper import find_projects_for_user, has_access
from ...db import db

from .. import project_bp
from ..forms import VulnerabilityForm


@project_bp.route('/<int:pid>/vulns/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def vulns(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)

    vulns = Vulnerability.query.filter(Vulnerability.project_id == project.id).all()

    return render_template('vulns/vulnlist.html', project=project, vulns=vulns,
                           title="Project Vulnerabilities")


@project_bp.route('/<int:pid>/vulns/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def vulns_add(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    form = VulnerabilityForm(project=pid)
    if form.validate_on_submit():
        vuln = Vulnerability(
            name=form.name.data,
            description=form.description.data,
            solution=form.solution.data,
            impact=form.impact.data,
            likelihood=form.likelihood.data,
            project=project,
            tags=form.tags.data,
            references=form.references.data,
            exploit_available=form.exploit_available.data
        )
        try:
            db.session.add(vuln)
            db.session.commit()
            flash('You have successfully added a new vulnerability.', 'success')
        except:
            flash('Error: vulnerability name already exists.', 'error')

        return redirect(url_for('projects.vulns', pid=project.id))

    vuln_templates = DefaultVuln.query.all()

    return render_template('vulns/add.html', project=project, vuln_templates=vuln_templates, form=form,
                               title="Add a new vulnerabilitiy")


@project_bp.route('/<int:pid>/vulns/addFromTemplate', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def vulns_add_from_template(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    return ""
