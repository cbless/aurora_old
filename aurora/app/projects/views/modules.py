from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ...db import db
from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.projects import Project, Module
from ...helper import has_access

from .. import project_bp
from ..helper import get_modules_for_project, get_members_from_project
from ..forms import ModuleForm, ModuleEditForm


@project_bp.route('/<int:pid>/modules/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def modules(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    modules = get_modules_for_project(pid)

    return render_template('modules/modulelist.html', project=project, modules=modules, title="Project Modules")


@project_bp.route('/<int:pid>/modules/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def add_module(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    project_members = get_members_from_project(project)
    form = ModuleForm()
    if request.method == 'POST' and form.validate_on_submit():
        module = Module(name=form.name.data,
                        description=form.description.data,
                        notes=form.notes.data,
                        status=form.status.data,
                        assigned_to=form.assigned_to.data,
                        project=project)
        try:
            db.session.add(module)
            db.session.commit()
            flash('You have successfully added a new module.', 'success')
        except:
            flash('Error: module name already exists.', 'error')

        return redirect(url_for('projects.modules', pid=project.id))

    return render_template('modules/module.html', project=project, form=form, title="Add Todo")




@project_bp.route('/<int:pid>/modules/edit/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def edit_module(pid, id):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    module = Module.query.get_or_404(id)
    form = ModuleEditForm()

    if form.validate_on_submit():
        form.populate_obj(module)
        try:
            db.session.add(module)
            db.session.commit()
            flash('You have successfully added a new todo item or task to project.', 'success')
        except:
            flash('Error: todo item / task already exists.', 'error')

        return redirect(url_for('projects.todos', pid=project.id))

    form.name.data = module.name
    form.description.data = module.description
    form.notes.data = module.notes
    form.status.data = module.status
    form.assigned_to.data = module.assigned_to
    return render_template('modules/module.html', form=form, project=project, title="Edit Todo")
