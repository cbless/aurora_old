from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ...db import db
from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.projects import Project, Module
from ...helper import has_access

from .. import project_bp
from ..helper import get_hosts_for_project


@project_bp.route('/<int:pid>/hosts/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def hosts(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    hosts = get_hosts_for_project(pid)

    return render_template('hosts/hostlist.html', project=project, hosts=hosts, title="List of hosts")
