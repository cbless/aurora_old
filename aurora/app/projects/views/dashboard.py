from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.projects import Project, Todo, Module
from ...models.vulnerabilities import Vulnerability
from ...models.systems import System, Host
from ...helper import has_access

from .. import project_bp


@project_bp.route('/<int:pid>/dashboard/', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def dashboard(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    modules = Module.query.filter(Module.project_id == project.id).all()

    vulns = Vulnerability.query.filter(Vulnerability.project_id == project.id).all()
    todos = Todo.query.filter(Todo.project_id == project.id).all()
    methodologies = project.methodologies

    systems = System.query.filter(System.project_id == project.id).all()
    hosts = Host.query.filter(Host.project_id == project.id).all()

    return render_template('project_dashboard.html', project=project, modules=modules,
                           vulns=vulns, methodologies=methodologies, systems=systems, hosts=hosts,
                           todos=todos, title="Project details")
