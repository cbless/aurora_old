from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ...db import db
from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.projects import Project, Module
from ...models.systems import System
from ...helper import has_access

from .. import project_bp
from ..helper import get_systems_for_project
from ..forms import SystemForm


@project_bp.route('/<int:pid>/systems/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def systems(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    systems = get_systems_for_project(pid)

    return render_template('systems/systemlist.html', project=project, systems=systems, title="List of systems")



@project_bp.route('/<int:pid>/systems/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def add_system(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    form = SystemForm(project=pid)
    if form.validate_on_submit():
        system = System(
            name=form.name.data,
            description=form.description.data,
            system_type=form.system_type.data,
            address=form.address.data,
            project=project
        )
        try:
            db.session.add(system)
            db.session.commit()
            flash('You have successfully added a new system.', 'success')
        except:
            flash("Error: Something went wrong, system wasn't created.", 'error')

        return redirect(url_for('projects.systems', pid=project.id))

    systems = System.query.all()

    return render_template('systems/system.html', project=project, systems=systems, form=form,
                               title="Add a new system")
