from flask import abort, flash, redirect, render_template, request, url_for, Response
from flask_security import login_required, roles_accepted, current_user
from docxtpl import DocxTemplate
import os

from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.projects import Project, Todo, Module
from ...models.vulnerabilities import Vulnerability
from ...models.systems import System, Host
from ...helper import has_access
from .. import project_bp

import jinja2
from jinja2 import Template

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../../.."))
template_dir = "{0}/reports/templates/".format(basedir)
report_dir = "{0}/reports/outdir/".format(basedir)

latex_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%%',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(basedir)
)


@project_bp.route('/<int:pid>/export/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def list_templates(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)

    templates = os.listdir(template_dir)

    return render_template('export/templatelist.html', project=project, templates=templates,
                           title="Available templates")


@project_bp.route('/<int:pid>/export/<template>', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def export(pid, template):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)

    if template not in os.listdir(template_dir):
        abort(403, "Unknown template.")
    modules = Module.query.filter(Module.project_id == project.id).all()

    vulns = Vulnerability.query.filter(Vulnerability.project_id == project.id).all()
    todos = Todo.query.filter(Todo.project_id == project.id).all()
    methodologies = project.methodologies

    systems = System.query.filter(System.project_id == project.id).all()
    hosts = Host.query.filter(Host.project_id == project.id).all()

    template = latex_jinja_env.get_template('/reports/templates/project-template.tex')

    output = template.render(current_user=current_user, project=project, modules=modules,
                            vulns=vulns, methodologies=methodologies, systems=systems, hosts=hosts,
                            todos=todos,)

    project_name = project.name.replace(" ","")
    return Response(
        output,
        mimetype="text/tex",
        headers={"Content-disposition": "attachment; filename=report.tex"})