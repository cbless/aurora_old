from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, DateField, BooleanField, HiddenField
from flask_ckeditor import CKEditorField
from wtforms_alchemy import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import DataRequired
from wtforms.fields.html5 import DateField

from ..models.accounts import User
from ..models.core import Status, ImpactLevel, LikelihoodLevel, Tag, Reference
from ..models.projects import Project
from ..models.systems import System, SystemType
from ..models.contacts import Company, Contact

from ..vars import ASSESSOR_ROLE_NAME, PM_ROLE_NAME


def pm_choices():
    return User.query.join(User.roles).filter(User.roles.any(name=PM_ROLE_NAME)).all()


def assessor_choices():
    return User.query.join(User.roles).filter(User.roles.any(name=ASSESSOR_ROLE_NAME)).all()


class ProjectForm(FlaskForm):
    """
    Form for admin to add or edit a department
    """
    name = StringField('Name', validators=[DataRequired()])
    start_date = DateField('Start Date', format='%Y-%m-%d')
    end_date = DateField('End date', format='%Y-%m-%d')
    status = QuerySelectField(query_factory=lambda: Status.query.all())

    company = QuerySelectField(query_factory = lambda: Company.query.all())

    customer = QuerySelectField(query_factory = lambda: Company.query.all())
    customer_pm = QuerySelectField(query_factory = lambda: Contact.query.all())
    customer_contacts = QuerySelectMultipleField(query_factory=lambda: Contact.query.all())

    project_manager = QuerySelectField(query_factory = pm_choices)
    assessors = QuerySelectMultipleField(query_factory= assessor_choices)

    description = CKEditorField('Description')
    note = CKEditorField('Notes')
    summary = CKEditorField('Summary')

    submit = SubmitField('Save')


class VulnerabilityForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    impact = QuerySelectField('Impact',
                              query_factory=lambda: ImpactLevel.query.all(),
                              render_kw = {'data-role': 'select2'})
    likelihood = QuerySelectField('Likelihood',
                                  query_factory=lambda: LikelihoodLevel.query.all(),
                                  render_kw = {'data-role': 'select2'})
    exploit_available = BooleanField('Exploit available')
    description = CKEditorField('Description')
    solution = CKEditorField('Solution')
    project = HiddenField('project')

    tags = QuerySelectMultipleField(query_factory=lambda: Tag.query.all())
    references = QuerySelectMultipleField(query_factory=lambda: Reference.query.all())

    submit = SubmitField('Save')


class VulnerabilityEditForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    impact = QuerySelectField('Impact',
                              query_factory=lambda: ImpactLevel.query.all(),
                              render_kw = {'data-role': 'select2'})
    likelihood = QuerySelectField('Likelihood',
                                  query_factory=lambda: LikelihoodLevel.query.all(),
                                  render_kw = {'data-role': 'select2'})
    exploit_available = BooleanField('Exploit available')
    description = CKEditorField('Description')
    solution = CKEditorField('Solution')
    #project = HiddenField('project')

    tags = QuerySelectMultipleField(query_factory=lambda: Tag.query.all())
    references = QuerySelectMultipleField(query_factory=lambda: Reference.query.all())

    submit = SubmitField('Save')


class TodoForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    description = CKEditorField('Description')
    notes = CKEditorField('Notes')
    status = QuerySelectField(query_factory=lambda: Status.query.all())

    assigned_to = QuerySelectField('Assigned to', query_factory=lambda: User.query.all())
    project = HiddenField('project')

    submit = SubmitField('Save')


class TodoEditForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    description = CKEditorField('Description')
    notes = CKEditorField('Notes')
    status = QuerySelectField(query_factory=lambda: Status.query.all())

    assigned_to = QuerySelectField('Assigned to', query_factory=lambda: User.query.all())
   # project = HiddenField('project')

    submit = SubmitField('Save')


class ModuleForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    description = CKEditorField('Description')
    notes = CKEditorField('Notes')
    status = QuerySelectField(query_factory=lambda: Status.query.all())

    assigned_to = QuerySelectField('Assigned to', query_factory=lambda: User.query.all())
    project = HiddenField('project')

    submit = SubmitField('Save')


class ModuleEditForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    description = CKEditorField('Description')
    notes = CKEditorField('Notes')
    status = QuerySelectField(query_factory=lambda: Status.query.all())

    assigned_to = QuerySelectField('Assigned to', query_factory=lambda: User.query.all())
    #project = HiddenField('project')

    submit = SubmitField('Save')


class SystemForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    system_type = QuerySelectField(query_factory=lambda: SystemType.query.all())

    address = StringField('Address')
    description = CKEditorField('Description')

    project = HiddenField('project')

    submit = SubmitField('Save')



class SystemEditForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    system_type = QuerySelectField(query_factory=lambda: SystemType.query.all())

    address = StringField('Address')
    description = CKEditorField('Description')

    #project = HiddenField('project')

    submit = SubmitField('Save')


