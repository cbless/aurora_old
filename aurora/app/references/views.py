from flask import render_template, flash, redirect, url_for, request
from flask_security import current_user, login_required, roles_accepted

from ..db import db
from ..vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, REFERENCE_ADMIN_ROLE_NAME
from ..models.core import Reference

from . import references_bp
from .forms import ReferenceForm


@references_bp.route('/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, REFERENCE_ADMIN_ROLE_NAME)
def list():
    references = Reference.query.all()

    return render_template('reference_list.html', references=references, title="References")


@references_bp.route('/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, REFERENCE_ADMIN_ROLE_NAME)
def create():
    form = ReferenceForm()
    if request.method == 'POST' and form.validate_on_submit():
        ref = Reference(name=form.name.data, link=form.link.data)
        try:
            db.session.add(ref)
            db.session.commit()
            flash('You have successfully added a new reference.', 'success')
        except:
            flash('Error: tag name already exists.', 'error')

        return redirect(url_for('references.list'))

    return render_template('reference.html', action="Add", add_ref=True, form=form, title="Add reference")


@references_bp.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, REFERENCE_ADMIN_ROLE_NAME)
def edit(id):
    ref = Reference.query.get_or_404(id)
    form = ReferenceForm(obj=ref)
    if form.validate_on_submit():
        ref.name = form.name.data
        ref.link=form.link.data
        try:
            db.session.add(ref)
            db.session.commit()
            flash('You have successfully modified a reference.', 'success')
        except:
            flash('Error: reference name already exists.', 'error')

        return redirect(url_for('references.list'))

    form.name.data = ref.name
    return render_template('reference.html', action="Add", add_ref=True, form=form, title="Add reference")


@references_bp.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, REFERENCE_ADMIN_ROLE_NAME)
def delete(id):
    ref = Reference.query.get_or_404(id)

    db.session.delete(ref)
    db.session.commit()
    flash('You have successfully deleted the reference.', 'success')

    # redirect to the reference page
    return redirect(url_for('references.list'))
