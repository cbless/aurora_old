from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class ReferenceForm(FlaskForm):
    """
    Form to add or edit a reference
    """
    name = StringField('Name', validators=[DataRequired()])
    link = StringField('Link', validators=[DataRequired()])
    submit = SubmitField('Save')