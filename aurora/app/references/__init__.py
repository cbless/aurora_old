from flask import Blueprint

references_bp = Blueprint('references', __name__, url_prefix='/references', template_folder='templates')

from .views import *

