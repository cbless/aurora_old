from flask import Blueprint, jsonify, g
from flask_restplus import Api

api_bp= Blueprint('api', __name__, url_prefix='/api/v1.0')
api = Api(api_bp, doc="/doc/",version="1.0", description="Aurora API")

from .resources.auth import UserLogin, UserLogout, TokenRefresh
from .resources.tags import TagCollection, TagItem
from .resources.users import UserCollection, UsersByProjectLookup, PMByProjectLookup
from .resources.references import ReferenceCollection

@api_bp.errorhandler
def default_error_handler(e):
    message = 'An unhandled exception occurred.'
    #log.exception(message)

    return {'message': message}, 500
