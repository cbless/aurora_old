from flask_restplus import fields
from . import api

tag_model = api.model('Tag', {
    'id': fields.Integer(type=int, description='The tag unique identifier'),
    'name': fields.String(type=str, required=True, description='The tag name')
})


user_Model = api.model('User', {
    'id' : fields.Integer(type= int, description='The users unique identifier'),
    'username': fields.String(type=str, required=True, description='The username')
})


login_model = api.model('LoginModel', {
    'access_token' : fields.String(type=str, required=True, description="The users access token"),
    'refresh_token' : fields.String(type=str, required=True, description="The users refresh token")
})


reference_model = api.model('Reference', {
    'name' : fields.String(type=str, required=True, description='The name of the reference'),
    'link' : fields.String(type=str, required=True, description='The URL of the reference')
})