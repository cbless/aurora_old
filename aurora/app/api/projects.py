from flask_security import current_user, login_required, roles_accepted
from flask import jsonify

from . import api

from ..helper import find_projects_for_user
from ..vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME

@api.route('/projects/list', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def list_projects():
    """
    List all projects the user has access to
    """
    user = current_user
    projects = find_projects_for_user(user)

    return jsonify(result={})
