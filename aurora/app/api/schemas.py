from marshmallow import fields

from .. import ma


class TagSchema(ma.Schema):
    id = fields.Integer(required=True)
    name = fields.String(required=True)

    _links = ma.Hyperlinks({
        'collection': ma.URLFor('api.tag_collection'),
        'self': ma.URLFor('api.tag_item',id='<id>')

    })
