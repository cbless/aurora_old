from flask_restplus import Resource, reqparse
from flask_restplus._http import HTTPStatus
from flask_jwt_extended import jwt_required

from ...models.accounts import User
from ...models.projects import Project
from ...projects.helper import get_members_from_project

from .. import api
from ..models import user_Model

ns = api.namespace("users", description='Operations related to users')


@ns.route("/")
class UserCollection(Resource):

    @ns.marshal_list_with(user_Model)
    @ns.response(HTTPStatus.OK, 'Returns a brief list of users.')
    @jwt_required
    def get(self):
        """ List all users"""
        users = User.query.all()
        return users, HTTPStatus.OK


user_project_parser = reqparse.RequestParser()
user_project_parser.add_argument('pid', type=int, help='Unique identifier of the project')


@ns.route("/allByProject/")
class UsersByProjectLookup(Resource):

    @ns.marshal_list_with(user_Model)
    @ns.response(HTTPStatus.OK, 'Returns a brief list of users, related to the given project.')
    @ns.response(HTTPStatus.BAD_REQUEST, "Validation error.")
    @ns.response(HTTPStatus.NOT_FOUND, "Project not found")
    @ns.expect(user_project_parser)
    @jwt_required
    def post(self):
        """ List all users related to the given project."""
        args = ns.payload
        pid = args['pid']
        project = Project.query.get_or_404(pid)
        users = get_members_from_project(project)
        return users, HTTPStatus.OK


@ns.route("/pmByProject/")
class PMByProjectLookup(Resource):

    @ns.marshal_list_with(user_Model)
    @ns.response(HTTPStatus.OK, 'Project manager for the given project.')
    @ns.response(HTTPStatus.BAD_REQUEST, "Validation error.")
    @ns.response(HTTPStatus.NOT_FOUND, "Project not found")
    @ns.expect(user_project_parser)
    @jwt_required
    def post(self):
        """ List all users related to the given project."""
        args = ns.payload
        pid = args['pid']
        project = Project.query.get_or_404(pid)
        pm = project.project_manager
        return pm, HTTPStatus.OK