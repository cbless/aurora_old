from flask_restplus import Resource, abort
from flask_restplus._http import HTTPStatus
from flask_jwt_extended import jwt_required

from ...db import db
from ...models.core import Tag

from .. import api
from ..models import tag_model


ns = api.namespace("tags", description='Operations related to tags')


@ns.route("/")
class TagCollection(Resource):

    @ns.marshal_list_with(tag_model)
    @ns.response(HTTPStatus.OK, 'Returns a list of tags')
    @jwt_required
    def get(self):
        """ List all tags"""
        tags = Tag.query.all()
        return tags, HTTPStatus.OK

    @ns.marshal_with(tag_model, code=HTTPStatus.CREATED, description='Object created')
    @ns.response(HTTPStatus.BAD_REQUEST, "Validation error or tag name already exists.")
    @ns.expect(tag_model, validate=True)
    @jwt_required
    def post(self):
        """Creates a new tag"""
        args = ns.payload
        tag = Tag(name=args['name'])
        try:
            db.session.add(tag)
            db.session.commit()
        except:
            abort(HTTPStatus.BAD_REQUEST, 'Tag already exists')

        return tag , HTTPStatus.CREATED


@ns.route("/lookup")
class TagLookup(Resource):

    @ns.marshal_list_with(tag_model)
    @ns.response(HTTPStatus.OK, 'Returns details of a tag')
    @ns.response(HTTPStatus.BAD_REQUEST, "Validation error.")
    @ns.expect(tag_model, validate=True)
    @jwt_required
    def post(self):
        """ List all tags that contains the the provided name substring"""
        args = ns.payload
        searchstring = args['name']
        tags = Tag.query.filter(Tag.name.contains(searchstring)).all()
        return tags, HTTPStatus.OK


@ns.route("/<int:id>")
@ns.param('id', 'The unique identifier for tags.')
@ns.response(HTTPStatus.NOT_FOUND, "Tag not found")
class TagItem(Resource):

    @ns.marshal_with(tag_model, code=HTTPStatus.OK, description='Success')
    @ns.response(HTTPStatus.NOT_FOUND, "Tag not found")
    @jwt_required
    def get(self,id):
        """Get the details about a single Tag."""
        tag = Tag.query.get_or_404(id)
        return tag, HTTPStatus.OK

    @ns.marshal_with(tag_model, code=HTTPStatus.OK, description='Success')
    @ns.response(HTTPStatus.NOT_FOUND, "Tag not found")
    @ns.response(HTTPStatus.BAD_REQUEST, "Validation error or tag name already exists.")
    @ns.expect(tag_model, validate=True)
    @jwt_required
    def put(self, id):
        """Updates the details about a single Tag."""
        tag = Tag.query.get_or_404(id)
        args = ns.payload
        tag.name = args["name"]

        try:
            db.session.add(tag)
            db.session.commit()
        except:
            abort(HTTPStatus.BAD_REQUEST, 'Tag already exists')

        return tag , HTTPStatus.OK

    @ns.response(HTTPStatus.NO_CONTENT, 'Tag deleted')
    @ns.response(HTTPStatus.NOT_FOUND, "Tag not found")
    @ns.response(HTTPStatus.BAD_REQUEST, "Can't delete Tag")
    @jwt_required
    def delete(self, id):
        """Delete a single Tag."""
        tag = Tag.query.get_or_404(id)
        try:
            db.session.delete(tag)
            db.session.commit()
        except:
            abort(HTTPStatus.BAD_REQUEST, "Can't delete Tag")

        return "", HTTPStatus.NO_CONTENT