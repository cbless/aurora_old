from flask_restplus import Resource, abort
from flask_restplus._http import HTTPStatus
from flask_jwt_extended import jwt_required

from ...db import db
from ...models.core import Reference

from .. import api
from ..models import reference_model


ns = api.namespace("references", description='Operations related to references')


@ns.route("/")
class ReferenceCollection(Resource):

    @ns.marshal_list_with(reference_model)
    @ns.response(HTTPStatus.OK, 'Returns a list of available references')
    @jwt_required
    def get(self):
        """ List all references. """
        refs = Reference.query.all()
        return refs, HTTPStatus.OK
