from flask import abort
from flask_restplus import Resource, reqparse
from flask_restplus._http import HTTPStatus
from flask_security.utils import verify_password
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required,
                                get_jwt_identity)

from jwt.exceptions import ExpiredSignatureError, InvalidTokenError
from flask_jwt_extended.exceptions import *

from ...models.accounts import User

from .. import api
from ..models import login_model

login_parser = reqparse.RequestParser()
login_parser.add_argument('username', type=str, required=True)
login_parser.add_argument('password', type=str, required=False)

ns = api.namespace("auth", description='Operations related to authentication')


@ns.route("/login/")
class UserLogin(Resource):

    @ns.marshal_with(login_model)
    @ns.response(HTTPStatus.OK, 'Success')
    @ns.response(HTTPStatus.UNAUTHORIZED, 'Login failed')
    @ns.expect(login_parser)
    def post(self):
        """Authenticate the user."""
        args = ns.payload
        try:
            user = User.query.filter(User.username==args['username']).first()
            if user:
                valid = verify_password(args['password'], user.password)
                if valid:

                    retval = {
                        "access_token": create_access_token(identity=user.id),
                        "refresh_token": create_refresh_token(identity=user.id)
                    }

                    return retval, HTTPStatus.OK
            abort(HTTPStatus.UNAUTHORIZED)
        except:
            abort(HTTPStatus.UNAUTHORIZED)



@ns.route("/logout/")
class UserLogout(Resource):

    @jwt_required
    def post(self):
        # TODO: create blacklist database table and add token to db
        return {'message': 'User logout'}


@ns.route("/refresh/")
class TokenRefresh(Resource):

    @jwt_refresh_token_required
    @ns.response(HTTPStatus.OK, 'Success')
    @ns.response(HTTPStatus.UNAUTHORIZED, 'Login failed')
    @ns.marshal_with(login_model)
    def post(self):
        """Refresh the users access_token."""
        try:
            current_user = get_jwt_identity()
            access_token = create_access_token(identity=current_user)
            return {'access_token': access_token}, HTTPStatus.OK
        except:
            abort(HTTPStatus.UNAUTHORIZED)




@api.errorhandler(NoAuthorizationError)
def handle_auth_error(e):
    return {'message': str(e)}, 401


@api.errorhandler(CSRFError)
def handle_auth_error(e):
    return {'message': str(e)}, 401


@api.errorhandler(ExpiredSignatureError)
def handle_expired_error(e):
    return {'message': 'Token has expired'}, 401


@api.errorhandler(InvalidHeaderError)
def handle_invalid_header_error(e):
    return {'message': str(e)}, 422


@api.errorhandler(InvalidTokenError)
def handle_invalid_token_error(e):
    return {'message': str(e)}, 422


@api.errorhandler(JWTDecodeError)
def handle_jwt_decode_error(e):
    return {'message': str(e)}, 422


@api.errorhandler(WrongTokenError)
def handle_wrong_token_error(e):
    return {'message': str(e)}, 422


@api.errorhandler(RevokedTokenError)
def handle_revoked_token_error(e):
    return {'message': 'Token has been revoked'}, 401


@api.errorhandler(FreshTokenRequired)
def handle_fresh_token_required(e):
    return {'message': 'Fresh token required'}, 401


@api.errorhandler(UserLoadError)
def handler_user_load_error(e):
    # The identity is already saved before this exception was raised,
    # otherwise a different exception would be raised, which is why we
    # can safely call get_jwt_identity() here
    identity = get_jwt_identity()
    return {'message': "Error loading the user {}".format(identity)}, 401


@api.errorhandler(UserClaimsVerificationError)
def handle_failed_user_claims_verification(e):
    return {'message': 'User claims verification failed'}, 400

