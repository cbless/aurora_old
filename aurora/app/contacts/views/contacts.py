from flask import abort, flash, redirect, render_template, request, url_for, jsonify
from flask_security import login_required, roles_accepted, current_user

from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.contacts import Contact
from ...db import db

from .. import contact_bp
from ..forms import ContactForm


@contact_bp.route('/contacts/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def contacts():

    contacts = Contact.query.all()

    return render_template('contacts/contactlist.html', contacts=contacts, title="Contacts")


@contact_bp.route('/contacts/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def add_contact():
    form = ContactForm()
    if request.method == 'POST' and form.validate_on_submit():
        contact = Contact(firstname=form.firstname.data,
                          title=form.title.data,
                          lastname=form.lastname.data,
                          phone=form.phone.data,
                          mobile=form.mobile.data,
                          email=form.email.data,
                          company=form.company.data,
                          street=form.street.data,
                          zipcode=form.street.data,
                          city=form.city.data,
                          notes=form.notes.data
                          )

        try:
            db.session.add(contact)
            db.session.commit()
            flash('You have successfully added a new contact.', 'success')
        except:
            flash('Error: contact could not be created.', 'error')

        return redirect(url_for('contacts.contacts'))

    return render_template('contacts/contact.html', form=form, title="Add contact")



@contact_bp.route('/contacts/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def edit_contact(id):
    contact = Contact.query.get_or_404(id)
    form = ContactForm(obj=contact)
    if form.validate_on_submit():
        form.populate_obj(obj=contact)
        try:
            db.session.add(contact)
            db.session.commit()
            flash('You have successfully modified a contact.', 'success')
        except:
            flash('Error: contact name already exists.', 'error')

        return redirect(url_for('contacts.contacts'))

    form.title.data = contact.title
    form.firstname.data = contact.firstname
    form.lastname.data = contact.lastname
    form.email.data = contact.email
    form.phone.data = contact.phone
    form.mobile.data = contact.mobile
    form.street.data = contact.street
    form.zipcode.data = contact.zipcode
    form.city.data = contact.city
    form.notes.data = contact.notes
    return render_template('contacts/contact.html', form=form, title="Edit contact")


@contact_bp.route('/contacts/lookup/<int:cid>', methods=['GET'])
@login_required
def lookup_by_company(cid):
    contacts = Contact.query.filter(Contact.company_id==cid).all()

    results = [ (c.id, str(c)) for c in contacts]

    return jsonify(results)