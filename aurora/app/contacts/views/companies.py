from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ...db import db
from ...vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...models.contacts import Company

from .. import contact_bp
from ..forms import CompanyForm


@contact_bp.route('/companies/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def companies():

    companies = Company.query.all()

    return render_template('companies/companylist.html', companies=companies, title="Companies")


@contact_bp.route('/companies/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def add_company():
    form = CompanyForm()
    if request.method == 'POST' and form.validate_on_submit():
        company = Company(name=form.name.data,
                          street=form.street.data,
                          zipcode=form.zipcode.data,
                          city=form.city.data,
                          notes=form.notes.data
                          )

        try:
            db.session.add(company)
            db.session.commit()
            flash('You have successfully added a new company.', 'success')
        except:
            flash('Error: company could not be created.', 'error')

        return redirect(url_for('contacts.companies'))

    return render_template('companies/company.html', form=form, title="Add company")


@contact_bp.route('/companies/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME)
def edit_company(id):
    company = Company.query.get_or_404(id)
    form = CompanyForm(obj=company)
    if form.validate_on_submit():
        form.populate_obj(obj=company)
        try:
            db.session.add(company)
            db.session.commit()
            flash('You have successfully modified a company.', 'success')
        except:
            flash('Error: company name already exists.', 'error')

        return redirect(url_for('contacts.companies'))

    form.name.data = company.name
    form.street.data = company.street
    form.zipcode.data = company.zipcode
    form.city.data = company.city
    form.notes.data = company.notes
    return render_template('companies/company.html', form=form, title="Edit company")
