from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ...models.accounts import User
from ...models.contacts import Contact
from ...db import db

from .. import contact_bp
from ..forms import ContactForm


@contact_bp.route('/profile/', methods=['GET', 'POST'])
@login_required
def profile():
    contact = current_user.contact

    if contact is None:
        # new contact
        form = ContactForm()
        if request.method == 'POST' and form.validate_on_submit():
            contact = Contact(title=form.title.data,
                              firstname=form.firstname.data,
                              lastname=form.lastname.data,
                              phone=form.phone.data,
                              mobile=form.mobile.data,
                              email=form.email.data,
                              company=form.company.data,
                              street=form.street.data,
                              zipcode=form.zipcode.data,
                              city=form.city.data,
                              notes=form.notes.data
                              )
            user = User.query.filter(User.id==current_user.id).first()
            user.contact_id = contact.id
            try:
                db.session.add(contact)
                db.session.add(user)
                db.session.commit()
                flash('You have successfully added a new profile to your user account.', 'success')
            except:
                flash("Error: Can't create contact.", 'error')

            return redirect(url_for('contacts.profile'))
        return render_template('profile/profile.html', form=form, title="Add Profile")
    else:
        # edit existing contact
        form = ContactForm(obj=contact)
        if form.validate_on_submit():
            contact.title = form.title.data
            contact.firstname = form.firstname.data
            contact.lastname = form.lastname.data
            contact.phone = form.phone.data
            contact.mobile = form.mobile.data
            contact.email = form.email.data
            contact.street = form.street.data
            contact.zipcode = form.zipcode.data
            contact.city = form.city.data
            contact.company = form.company.data
            try:
                db.session.add(contact)
                db.session.commit()
                flash('You have successfully modified your profile.', 'success')
            except:
                flash("Error: profile can't be modified.", 'error')

            return redirect(url_for('contacts.profile'))

        form.title.data = contact.title
        form.firstname.data = contact.firstname
        form.lastname.data = contact.lastname
        form.phone.data = contact.phone
        form.mobile.data = contact.mobile
        form.email.data = contact.email
        form.street.data = contact.street
        form.zipcode.data = contact.zipcode
        form.city.data = contact.city
        form.company.data = contact.company
        return render_template('profile/profile.html', form=form, title="Edit Profile")