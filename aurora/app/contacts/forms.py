from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, DateField, BooleanField, HiddenField
from flask_ckeditor import CKEditorField
from wtforms_alchemy import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import DataRequired, Length, Email

from ..models.contacts import Company


class CompanyForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    street = StringField('Street', validators=[Length(max=400)])
    number = StringField('Number', validators=[Length(max=10)])
    zipcode = StringField('Zip', validators=[Length(max=10)])
    city = StringField('City', validators=[Length(max=255)])

    notes = CKEditorField('Notes')

#    addresses = QuerySelectField('Addresses', query_factory=lambda: Address.query.all())

    submit = SubmitField('Save')


class ContactForm(FlaskForm):
    title = StringField('Title', validators=[Length(max=10)])
    firstname = StringField('Firstname', validators=[Length(max=255)])
    lastname = StringField('Lastname', validators=[DataRequired(), Length(max=255)])

    phone = StringField('Phone', validators=[Length(max=255)])
    mobile = StringField('Mobile', validators=[Length(max=255)])
    email = StringField('EMail', validators=[Length(max=2048), Email()])

    company = QuerySelectField('Company', query_factory=lambda: Company.query.all())

    street = StringField('Street', validators=[Length(max=400)])
    zipcode = StringField('Zip', validators=[Length(max=10)])
    city = StringField('City', validators=[Length(max=255)])

    notes = CKEditorField('Notes')
#    addresses = QuerySelectField('Addresses', query_factory=lambda: Address.query.all())

    submit = SubmitField('Save')

