from flask import render_template
from flask_security import login_required, current_user

from ..helper import find_projects_for_user
from ..models.projects import Todo, Module

from . import main


@main.route('/', methods=['GET'])
@login_required
def index():
    projects = find_projects_for_user(current_user)
    modules = Module.query.filter(Module.assigned_to_id==current_user.id).all()
    todos = Todo.query.filter(Todo.assigned_to_id == current_user.id).all()
    return render_template('index.html', projects=projects, modules=modules, todos=todos)
